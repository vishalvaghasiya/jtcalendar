//
//  ViewController.swift
//  JTCalendarSwift
//
//  Created by KMSOFT on 13/04/17.
//  Copyright © 2017 KMSOFT. All rights reserved.
//

import UIKit
import JTCalendar
class ViewController: UIViewController ,  JTCalendarDelegate {
    
    @IBOutlet var calendarView: UIView!
    @IBOutlet var calendarMenuView: JTCalendarMenuView!
    @IBOutlet var calendarContentView: JTHorizontalCalendarView!
    var calendarManager:JTCalendarManager! = JTCalendarManager()
    
    @IBOutlet var selectedTextField: UITextField!
    var eventsByDate = NSMutableDictionary()
    var dateSelected:Date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendarManager = JTCalendarManager()
        calendarManager.delegate = self
        calendarMenuView.contentRatio = 0.75
        calendarManager.settings.weekDayFormat = JTCalendarWeekDayFormat.single
        
        calendarManager.menuView = self.calendarMenuView
        calendarManager.contentView = self.calendarContentView
        calendarManager.setDate(Date())
        
        self.calendarView.isHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectedDateClick(_ sender: UITextField) {
        
        UIView.animate(withDuration: 0.8, delay: 0.8, options: [], animations: {
            self.calendarView.isHidden = false
        }, completion: nil)
    }
    
    //MARK: - CalendarManager delegate
    func calendar(_ calendar: JTCalendarManager!, prepareDayView dayView: UIView!) {
        
        let dayView = dayView as! JTCalendarDayView
        dayView.isHidden = false
        // Other month
        if (dayView.isFromAnotherMonth) {
            dayView.isHidden = false
            dayView.textLabel.textColor = UIColor.gray
        }
            // Today
        else if (calendarManager.dateHelper.date(Date(), isTheSameDayThan: dayView.date!)){
            dayView.circleView.isHidden = false
            dayView.dotView.isHidden = false
            dayView.circleView.backgroundColor = UIColor.blue
            dayView.dotView.backgroundColor = UIColor.white
            dayView.textLabel.textColor = UIColor.white
            
        }
            // Selected date
        else if (((dateSelected != nil)) && calendarManager.dateHelper.date(dateSelected as Date!, isTheSameDayThan: dayView.date!)){

            dayView.circleView.isHidden = false
            dayView.dotView.isHidden = false
            dayView.circleView.backgroundColor = UIColor.red
            dayView.dotView.backgroundColor = UIColor.white
            dayView.textLabel.textColor = UIColor.white
        }
            // Another day of the current month
        else {
            dayView.circleView.isHidden = true
            dayView.dotView.isHidden = true
            dayView.dotView.backgroundColor = UIColor.red
            dayView.textLabel.textColor = UIColor.black
        }
    }
    
    func calendar(_ calendar: JTCalendarManager!, didTouchDayView dayView: UIView!) {
        
        let dayView = dayView as! JTCalendarDayView
        self.dateSelected = dayView.date! as Date
        self.selectedTextField.text = "\(self.getFormattedDate(string: "\(self.dateSelected)"))"
//        selectedTextField.text = "\(self.dateSelected!)"
        // Animation for the circleView
        dayView.circleView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.transition(with: dayView, duration: 0.3, options: UIViewAnimationOptions.allowUserInteraction, animations: {
            dayView.circleView.transform = CGAffineTransform.identity
            self.calendarManager.reload()
        }, completion: nil)
        
        // Don't change page in week mode because block the selection of days in first and last weeks of the month
        if (calendarManager.settings.weekModeEnabled) {
            return
        }
        
        // Load the previous or next page if touch a day from another month
        if (!calendarManager.dateHelper.date(calendarContentView.date, isTheSameMonthThan: dayView.date)) {
            if (calendarContentView.date.compare(dayView.date) == .orderedAscending) {
                calendarContentView.loadNextPageWithAnimation()
            }
            else{
                calendarContentView.loadPreviousPageWithAnimation()
            }
        }
    }
    
    // MARK: - Date Formater
    func getFormattedDate(string: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz"
        let formateDate = dateFormatter.date(from: string)!
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: formateDate)
    }
}

