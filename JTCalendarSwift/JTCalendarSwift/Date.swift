//
//  Date.swift
//  JTCalendarSwift
//
//  Created by KMSOFT on 13/04/17.
//  Copyright © 2017 KMSOFT. All rights reserved.
//

import Foundation

extension Date {
    
    static func date(string : String,format: String) -> Date {
        let dateFormatter : DateFormatter = DateFormatter();
        dateFormatter.dateFormat = format;
        return dateFormatter.date(from: string)!
    }
    
    func string(format : String) -> String {
        let dateFormatter : DateFormatter = DateFormatter();
        dateFormatter.dateFormat = format;
        return dateFormatter.string(from: self);
    }

    
}
